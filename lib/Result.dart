import 'package:flutter/material.dart';

class Result extends StatelessWidget{
  final int number;
  final Function resetHandler;

  Result(this.number, this.resetHandler);


  @override
  Widget build(BuildContext context) {
    
    return 
      Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0),

      child: Column(      
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Yes, you did it',          
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.lightGreen,
            fontFamily: 'Calibri-Semibold',
            fontSize: 25,                        
          ),          
          ),
        Text(
          number.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.lightGreen,
            fontFamily: 'Calibri-Semibold',
            fontSize: 120.0,
            letterSpacing: 1,                        
          ),
        ),
        

        RaisedButton(
          child: Text(
            'TRY AGAIN',                         
            style: TextStyle(
              color: Colors.white,
              fontSize: 23,
              fontFamily: "Calibre-Semibold",
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
          padding: EdgeInsets.symmetric(horizontal: 100.0, vertical: 12.0,),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: Colors.blue,
          onPressed: resetHandler,
        ),
      ],
      ),
    );
  }
}