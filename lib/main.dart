import 'package:flutter/material.dart';
import 'dart:math';
import './Result.dart';


void main() =>runApp( MaterialApp(
  home: MyApp(),
  debugShowCheckedModeBanner: false,
));

class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{

  var _number = Random().nextInt(100)==0 ? 1 :Random().nextInt(100);
  var _attempts = 0;
  var _userNumber = 0; 
  var myController = TextEditingController();

  String get _textHelp{

    var _textHelp = "Let see how lucky you are";
        
    if (_number == _userNumber){
      if (_attempts == 1) {
        _textHelp = 'You must play the lottery';
      }
      _textHelp = 'Yes, you did it!';           
    } else if (_number > _userNumber){
      _textHelp = 'Try with a higher number';
    } else{
      _textHelp = 'Try with a smaller number';
    }

     if (_attempts == 0){
      _textHelp = "Let see how lucky you are";
    }

    return _textHelp;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();    
  }

  void _resetGame(){
    setState(() {
      _number = Random().nextInt(100);
      _userNumber = 0; 
      _attempts = 0; 
      myController = TextEditingController();        
    });
  }

  void _incrementAttempts(){    
    setState(() {
      _attempts++;      
    });
    print(_number);
    if (_number == _userNumber){
      if (_attempts == 1) {
        print('You must play the lottery');
      }
      print('Yes, you did it!');            
    } else if (_number > _userNumber){
      print('Try with a higher number');
    } else{
      print('Trye with a smaller number');
    }

  }

  @override
  Widget build(BuildContext context) {
  
    return Scaffold(       
      backgroundColor: Color(0xFF2d3447),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 12.0, right: 12.0, top:30.0, bottom:8.0),
              child: Column(
                  children: <Widget>[

                  Text('Guess the Number',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 40.0,
                        fontFamily: "Calibre-Semibold",
                        letterSpacing: 1.0,                     
                      ),                      
                      ),

                   Text(
                      'between 1 to 100 ',
                      style: TextStyle(
                            color: Colors.white,
                            fontSize: 30.0,
                            fontFamily: "Calibre-Semibold",
                            letterSpacing: 1,
                            ),                       
                    ),
                  ],
              ),
            ),
            SizedBox(height: 50),

            _number == _userNumber ? Result(_number,_resetGame):

            Center(              
              child: Column(
                children: <Widget>[   

                    SizedBox(height: 5),
                    Text( _textHelp.toString() ,
                      style: TextStyle(
                        color: Colors.lightGreen,
                        fontSize: 25.0,
                        fontFamily: "Calibre-Semibold",
                        letterSpacing: 1.0,                                            
                      ), 
                      textAlign: TextAlign.center,
                                       
                      ),

                    SizedBox(height: 20),
                    
                    TextField(
                      controller: myController,
                      keyboardType: TextInputType.number,
                      maxLength: 3,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 115.0),                                                
                      ),
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 90.0,
                        fontFamily: "Calibre-Semibold",
                        letterSpacing: 1.0,                         
                      ),
                      maxLines: 1,                      
                    ),                 

                    SizedBox(height: 15,),
                    Text('Failed attempts : $_attempts',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 18.0,
                        fontFamily: "Calibre-Semibold",
                        letterSpacing: 1.0,                     
                      ), 
                      textAlign: TextAlign.center,                     
                      ),                                     
                                       
                    SizedBox(height: 20), 
                   // Result(_number,_userNumber),                   
                   
                    RaisedButton(
                      child: Text(
                        '    TRY   ',                                                 
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 23,
                          fontFamily: "Calibre-Semibold",
                          letterSpacing: 1,
                          ),
                        ),
                      padding: EdgeInsets.symmetric(horizontal: 100.0, vertical: 12.0,),
                      color: Colors.blue,
                      onPressed: () {
                        _userNumber =  int.parse(myController.text);
                        print(_userNumber);
                        _incrementAttempts();
                        },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)
                        ),
                    ), 
                                       
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


